<?php
/**
 * ApiTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Api;
use civicrm_api3;
use stdClass;

/**
 * Tests for the Api class.
 */
class ApiTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();
        $civicrmApi = new civicrm_api3();
        $civicrmApi->foo = new civicrm_api3();
        $civicrmApi->result = new stdClass();
        $civicrmApi->values = [];
        $this->api = new Api($civicrmApi);
    }

    /**
     * Test the getInstance() function.
     *
     * @return void
     */
    public function testGetInstance()
    {
        $api = Api::getInstance();
        $this->assertInstanceOf(Api::class, $api);
    }

    /**
     * Test the getInstance() function.
     *
     * @return void
     * @expectedException Exception
     * @runInSeparateProcess
     */
    public function testGetInstanceWithoutPath()
    {
        Api::$path = null;
        $api = Api::getInstance();
        $this->assertInstanceOf(Api::class, $api);
    }


    /**
     * Test the getSingle() function.
     *
     * @return void
     */
    public function testGetSingle()
    {
        $this->assertInstanceOf(StdClass::class, $this->api->getSingle('foo', ['foo' => 'bar']));
    }

    /**
     * Test the getSingle() function with an API error.
     *
     * @return void
     * @expectedException CivicrmApi\ApiException
     */
    public function testGetSingleWithError()
    {
        $this->api->getSingle('foo', []);
    }

    /**
     * Test the getOptions() function.
     *
     * @return void
     */
    public function testGetOptions()
    {
        $this->assertInternalType('array', $this->api->getOptions('foo', 'goodtype'));
    }

    /**
     * Test the getOptions() function with an API error.
     *
     * @return void
     * @expectedException CivicrmApi\ApiException
     */
    public function testGetOptionsWithError()
    {
        $this->api->getOptions('foo', 'unknowntype');
    }

    /**
     * Test the get() function.
     *
     * @return void
     */
    public function testGet()
    {
        $this->assertInternalType('array', $this->api->get('foo', ['foo' => 'bar']));
    }

    /**
     * Test the get() function with an API error.
     *
     * @return void
     * @expectedException CivicrmApi\ApiException
     */
    public function testGetWithError()
    {
        $this->api->get('foo', ['wrong_constraint']);
    }
}
