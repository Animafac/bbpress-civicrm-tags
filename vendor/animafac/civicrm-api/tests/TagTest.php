<?php
/**
 * ContactTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Api;
use CivicrmApi\Tag;
use civicrm_api3;
use stdClass;

/**
 * Tests for the Contact class.
 */
class TagTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $civicrmApi = new civicrm_api3();
        $civicrmApi->Tag = new civicrm_api3();
        $tag = new stdClass();
        $tag->id = 1;
        $civicrmApi->values = [
            $tag,
            $tag
        ];
        $civicrmApi->result = new stdClass();
        $civicrmApi->result->name = 'tag_name';
        $civicrmApi->result->id = 3;

        $api = new Api($civicrmApi);
        Api::setInstance($api);

        $this->tag = new Tag(3);
    }

    /**
     * Test the get() function.
     *
     * @return void
     */
    public function testGet()
    {
        $this->assertEquals(3, $this->tag->get('id'));
        $this->assertEquals('tag_name', $this->tag->get('name'));
    }

    /**
     * Test the getAll() function.
     *
     * @return void
     */
    public function testGetAll()
    {
        foreach (Tag::getAll() as $tag) {
            $this->assertInstanceOf(Tag::class, $tag);
        }
    }

    /**
     * Test the getAllTagsets() function.
     *
     * @return void
     */
    public function testGetAllTagsets()
    {
        foreach (Tag::getAllTagsets() as $tag) {
            $this->assertInstanceOf(Tag::class, $tag);
        }
    }

    /**
     * Test the getChildren() function.
     *
     * @return void
     */
    public function testGetChildren()
    {
        foreach ($this->tag->getChildren() as $tag) {
            $this->assertInstanceOf(Tag::class, $tag);
        }
    }
}
