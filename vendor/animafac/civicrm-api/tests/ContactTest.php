<?php
/**
 * ContactTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Api;
use CivicrmApi\Contact;
use civicrm_api3;
use stdClass;

/**
 * Tests for the Contact class.
 */
class ContactTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $civicrmApi = new civicrm_api3();
        $civicrmApi->Contact = new civicrm_api3();
        $civicrmApi->Website = new civicrm_api3();
        $civicrmApi->result = new stdClass();
        $civicrmApi->result->foo = 'bar';
        $civicrmApi->result->id = 3;
        $civicrmApi->result->url = 'http://example.com';

        $civicrmApi->values = [
            $this->getMockWebsiteType('goodwebsite', 1),
            $this->getMockWebsiteType('badwebsite', 2)
        ];

        $api = new Api($civicrmApi);
        Api::setInstance($api);
        $this->contact = new Contact(3);
    }

    /**
     * Test the get() function.
     *
     * @return void
     */
    public function testGet()
    {
        $this->assertEquals('bar', $this->contact->get('foo'));
        $this->assertEquals(3, $this->contact->get('id'));
        $this->assertNull($this->contact->get('unknownproperty'));
    }

    /**
     * Test the getWebsiteTypes() function.
     *
     * @return void
     */
    public function testGetWebsiteTypes()
    {
        $this->assertEquals(['goodwebsite', 'badwebsite'], $this->contact->getWebsiteTypes());
    }

    /**
     * Test the getWebsite() function.
     *
     * @return void
     */
    public function testGetWebsite()
    {
        $this->assertEquals('http://example.com', $this->contact->getWebsite('goodwebsite'));
        $this->assertEquals('', $this->contact->getWebsite('badwebsite'));
    }

    /**
     * Test the getWebsite() function with an unkown website type.
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetWebsiteWithError()
    {
        $this->contact->getWebsite('unknownwebsite');
    }
}
