<?php
/**
 * Api class.
 */

namespace CivicrmApi;

use Exception;
use civicrm_api3;
use stdClass;

/**
 * Access the CiviCRM API.
 */
class Api
{

    /**
     * CiviCRM API object.
     * @var civicrm_api3
     */
    private $api;

    /**
     * Path to CiviCRM config.
     * @var string
     */
    public static $path;

    /**
     * Api class instance.
     * @var Api
     */
    private static $instance;

    /**
     * Api constructor.
     * @param civicrm_api3 $api CiviCRM API object
     */
    public function __construct(civicrm_api3 $api = null)
    {
        if (isset($api)) {
            $this->api = $api;
        } else {
            if (!isset(self::$path)) {
                throw new Exception('You need to define CivicrmApi\Api::$path.');
            }
            $this->api = new civicrm_api3(['conf_path' => self::$path]);
        }
    }

    /**
     * Get an Api class instance.
     *
     * @return Api
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * Set the current Api instance.
     *
     * @param Api $instance Api
     */
    public static function setInstance(self $instance)
    {
        self::$instance = $instance;
    }

    /**
     * Get a single object of the specified type.
     *
     * @param string $entity      CiviCRM type (Contact, etc.)
     * @param array  $constraints Filters used to select the object
     *
     * @return stdClass
     */
    public function getSingle($entity, array $constraints)
    {
        if ($this->api->$entity->getsingle($constraints)) {
            return $this->api->result;
        } else {
            throw new ApiException($this->api->errorMsg());
        }
    }

    /**
     * Get available options for one of the fields of the specified type.
     *
     * @param string $entity CiviCRM type (Contact, etc.)
     * @param string $field  Field to get options for
     *
     * @return array
     */
    public function getOptions($entity, $field)
    {
        if ($this->api->$entity->getoptions(['field' => $field])) {
            return $this->api->values;
        } else {
            throw new ApiException($this->api->errorMsg());
        }
    }

    /**
     * Get objects of the specified type.
     *
     * @param string $entity      CiviCRM type (Contact, etc.)
     * @param array  $constraints Filters used to select the objects
     *
     * @return stdClass[]
     */
    public function get($entity, array $constraints = [])
    {
        if ($this->api->$entity->get($constraints)) {
            return $this->api->values;
        } else {
            throw new ApiException($this->api->errorMsg());
        }
    }
}
