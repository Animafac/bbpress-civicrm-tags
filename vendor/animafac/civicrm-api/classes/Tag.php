<?php
/**
 * Tag class.
 */

namespace CivicrmApi;

/**
 * Manage CiviCRM tags.
 */
class Tag extends ApiObject
{
    /**
     * Tag constructor.
     *
     * @param int $id CiviCRM tag ID
     */
    public function __construct($id)
    {
        parent::__construct('id', $id);
    }

    /**
     * Get all tags with the specified parent.
     * @return Tag[]
     */
    public function getChildren()
    {
        return self::getAll([
            'parent_id' => $this->get('id'),
            'is_tagset' => false
        ]);
    }

    /**
     * Get all tagsets with no parent.
     * @return Tag[]
     */
    public static function getAllTagsets()
    {
        return self::getAll(['is_tagset' => true]);
    }
}
