<?php
/**
 * ApiObject class.
 */

namespace CivicrmApi;

use stdClass;

/**
 * Manage CiviCRM objects.
 */
abstract class ApiObject
{
    /**
     * Contact object returned by the CiviCRM API.
     * @var stdClass
     */
    private $apiObject;

    /**
     * Api class instance.
     * @var Api
     */
    protected $api;

    /**
     * List of properties that need to be casted as integer.
     * @var array
     */
    const INT_PROPERTIES = ['id', 'contact_id'];

    /**
     * ApiObject constructor.
     * @param string $field Name of the field containing the ID
     * @param int    $id    Object ID
     */
    public function __construct($field, $id)
    {
        $this->api = Api::getInstance();
        $this->setApiObject($this->api->getSingle(self::getShortClass(), [$field => $id]));
    }

    /**
     * Get a contact property
     *
     * @param string $name Name of the property
     *
     * @return mixed Value of the property
     */
    public function get($name)
    {
        if (isset($this->apiObject->$name)) {
            if (in_array($name, self::INT_PROPERTIES)) {
                return (int) $this->apiObject->$name;
            }

            return $this->apiObject->$name;
        }
    }

    /**
     * Get the current class name without any namespace.
     * @return string Class name
     */
    private static function getShortClass()
    {
        $classname = get_called_class();
        return substr($classname, strrpos($classname, '\\') + 1);
    }

    /**
     * Get all objects from this type.
     * @return ApiObject[]
     */
    public static function getAll(array $constraint = [])
    {
        $api = Api::getInstance();
        $tags = [];

        foreach ($api->get(self::getShortClass(), $constraint) as $object) {
            $class = get_called_class();
            $tag = new $class($object->id);
            $tags[] = $tag;
        }

        return $tags;
    }

    /**
     * Manually set the API object.
     * @param stdClass $object API object
     */
    private function setApiObject(stdClass $object)
    {
        $this->apiObject = $object;
    }
}
