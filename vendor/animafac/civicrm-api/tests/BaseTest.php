<?php
/**
 * BaseTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Api;
use PHPUnit\Framework\TestCase;
use Mockery;
use civicrm_api3;
use stdClass;

/**
 * Class used to create mocks common to every test class.
 */
abstract class BaseTest extends TestCase
{

    /**
     * Create mock classes.
     */
    protected function setUp()
    {
        Mockery::mock('overload:civicrm_api3')
            ->shouldReceive('getsingle')
            ->with(['foo' => 'bar'])->andReturn(true)
            ->shouldReceive('getsingle')
            ->with([])->andReturn(false)
            ->shouldReceive('getsingle')
            ->with(['contact_id' => 3, 'website_type_id' => 2])
            ->andReturn(false)
            ->shouldReceive('getsingle')->andReturn(true)

            ->shouldReceive('getoptions')
            ->with(['field' => 'goodtype'])->andReturn(true)
            ->shouldReceive('getoptions')
            ->with(['field' => 'unknowntype'])->andReturn(false)
            ->shouldReceive('getoptions')->andReturn(true)

            ->shouldReceive('get')
            ->with(['foo' => 'bar'])->andReturn(true)
            ->shouldReceive('get')
            ->with([])->andReturn(true)
            ->shouldReceive('get')
            ->with(['wrong_constraint'])->andReturn(false)
            ->shouldReceive('get')
            ->with(['parent_id' => 3, 'is_tagset' => false])->andReturn(true)
            ->shouldReceive('get')
            ->with(['is_tagset' => true])->andReturn(true)

            ->shouldReceive('errorMsg');

        Api::$path = 'foo';
    }

    /**
     * Destroy mock classes.
     *
     * @return void
     */
    protected function tearDown()
    {
        Mockery::close();
    }

    /**
     * Generate a mock CiviCRM WebsiteType object.
     *
     * @param string  $value Type name.
     * @param integer $key   Type ID.
     *
     * @return stdClass
     */
    protected function getMockWebsiteType($value, $key = 1)
    {
        $websiteType = new stdClass();
        $websiteType->value = $value;
        $websiteType->key = $key;

        return $websiteType;
    }
}
