<?php
/*
Plugin Name: bbPress CiviCRM Tags
Version: 0.1.0
Author: Animafac
Author URI: https://www.animafac.net/
Plugin URI: https://framagit.org/Animafac/bbpress-civicrm-tags
Description: Use CiviCRM tags in bbPress
 */

use BbpressCivicrmTags\Main;
use CivicrmApi\Api;

require_once __DIR__.'/vendor/autoload.php';

if (defined('WP_PLUGIN_DIR')) {
    require_once WP_PLUGIN_DIR.'/civicrm/civicrm/api/class.api.php';

    $uploadDir = wp_upload_dir();
    Api::$path = $uploadDir['basedir'].'/civicrm/';

    add_action('bbp_theme_after_topic_form_content', [Main::class, 'addFields']);
    add_action('bbp_new_topic', [Main::class, 'saveFields']);
    add_action('bbp_edit_topic', [Main::class, 'saveFields']);
    add_action('bbp_template_before_replies_loop', [Main::class, 'displayFields']);
}
